/**
    * Represents a numeric entry on our cell grid.
    * Any similarity between the name and other technologies is unintentional ;)
*/
function Node(element)
{
    var value = 2, domElement = element;
    
    this.getValue       = function(){return value;}
    this.setValue       = function(newValue)
    {
        value = newValue;
        if (domElement != null) domElement.html(value);
    }
    this.getDomElement  = function(){return domElement;}
    
    return this;
}