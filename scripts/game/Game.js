/**
 *  The orchestrator of our game, which controls moves and the grid
 *  itself. Basically, it's a facade that encapsulates the complexity
 *  of the game, to keep the code that relies on this component clean.
 */
function Game(grid, messageViewers)
{
    var cells = grid, lastMove = null, reference = this;
    
    /* Starts the game */
    this.start = function()
    {
        reference.randomGenerate();
        return reference;
    }
    
    /* Finishes the game */
    this.over = function()
    {
        var maxScore = 0;
        
        for(var counter = 0, size = grid.length;counter < size;counter++)
            for(var counter2 = 0, size2 = grid[counter].length;counter2 < size2;counter2++)
                if(grid[counter][counter2].getNode() != null && grid[counter][counter2].getNode().getValue() > maxScore)
                    maxScore = grid[counter][counter2].getNode().getValue();
        
        messageViewers.html(['Game over, greatest block: <strong>', maxScore, '</strong>.'].join());
        return references;
    }
    
    /* Checks if the game is over (that means: without any allowable move) */
    this.isMoveable = function()
    {
        var returnData = false;
        
        for(var counter = 0, size = grid.length;counter < size;counter++)
        {
            for(var counter2 = 0, size2 = grid[counter].length;counter2 < size2;counter2++)
            {
                if (grid[counter][counter2].getNode() == null)
                {
                    returnData = true;
                    break;
                }
                
                /* The siblings are the Node entries above, below and aside the current Node */
                var siblings =
                [
                    {top: counter - 1, left: counter2},
                    {top: counter + 1, left: counter2},
                    {top: counter, left: counter2 - 1},
                    {top: counter, left: counter2 + 1}
                ];
             
                /* Check if any sibling is empty or is mergeable */   
                for(var counter3 = 0, size3 = siblings.length;counter3 < size3;counter3++)
                {
                    var sibling = siblings[counter3];
                    
                    if (grid[sibling.top] == null || grid[sibling.top][sibling.left] == null)continue;
                    if (grid[sibling.top][sibling.left].getNode() == null || grid[sibling.top][sibling.left].getNode().getValue() == grid[counter][counter2].getNode().getValue())
                    {
                        returnData = true;
                        break;
                    }
                }
            }
        }
        
        return returnData;
    }
    
    /* Moves the nodes around the grid for a defined direction */
    this.move = function(direction)
    {
        if (grid.length == 0)return;
        
        var directionsConfig =
        {
            up   : {left: 0, top: -1, loop: {top: {start: 0, end: grid.length - 1, increment: 1}, left: {start: 0, end: grid[0].length - 1, increment: 1}}},
            down : {left: 0, top:  1, loop: {top: {start: grid.length - 1, end: 0, increment:-1}, left: {start: 0, end: grid[0].length - 1, increment: 1}}},
            left : {left:-1, top:  0, loop: {top: {start: 0, end: grid.length - 1}, left: {start: 0, end: grid[0].length - 1, increment: 1}}},
            right: {left: 1, top:  0, loop: {top: {start: 0, end: grid.length - 1}, left: {start: grid[0].length - 1, end: 0, increment: -1}}},
            checker: function(index, loop){return loop.increment > 0 ? index <= loop.end : index >= loop.end;}
        }, moved = false;
        
        /* Ensure an existing direction */
        if(!(direction in directionsConfig)) return;
        var directions = directionsConfig[direction];
        
        /* Setup the looping strategy for rows and cols */
        directions.loop.top = $.extend({start: 0, end: grid.length, increment: 1},directions.loop.top);
        directions.loop.left= $.extend({start: 0, end: grid[0].length, increment: 1},directions.loop.left);
        
        /**
         *  When moving the blocks, we must ensure to make it in the correct order.
         *  When moving to the left, per example, we must start from the blocks on the right.
         */
        for(var counter = directions.loop.top.start, size = directions.loop.top.end;directionsConfig.checker(counter, directions.loop.top);counter += directions.loop.top.increment)
        {
            for(var counter2 = directions.loop.left.start, size2 = directions.loop.left.end;directionsConfig.checker(counter2, directions.loop.left);counter2 += directions.loop.left.increment)
            {
                if (grid[counter][counter2].getNode() == null)continue;
                
                var cell        = grid[counter][counter2],
                    destination = {top: counter, left: counter2};
                
                /* Define the target position */
                for(var top = destination.top, left = destination.left;grid[top] != null && grid[top][left] != null;top += directions.top, left += directions.left)
                {
                    /* Skips the current node */
                    if (top == counter && left == counter2)continue;
                    /* Stops on filled grid points */
                    if (grid[top][left].getNode() != null)break;
                    
                    destination.top     = top;
                    destination.left    = left;
                }
                
                /* Try to merge the Nodes */
                if(merge(grid, grid[counter][counter2], {top: destination.top + directions.top, left: destination.left + directions.left})){moved = true;continue;}
                
                /* Let's move on! */
                if (destination.top != counter || destination.left != counter2)
                {
                    var move = {source: {top: counter, left: counter2}, target: {top: destination.top, left: destination.left}, direction: direction};
                    grid[move.source.top][move.source.left].getNode().getDomElement().appendTo(grid[move.target.top][move.target.left].getDomElement());
                    grid[move.target.top][move.target.left].setNode(grid[move.source.top][move.source.left].getNode());
                    grid[move.source.top][move.source.left].setNode(null);
                    moved = true;
                }
            }
        }
        
        var generated = false;
        if (moved)generated = reference.randomGenerate();
        if(!generated && !reference.isMoveable())reference.over();
        return;
    }
    
    /* Generates a Node inside an empty Cell randomly, if possible */
    this.randomGenerate = function()
    {
        var free = [];
        
        /* We must find only the empty cells */
        for(var counter = 0, size = cells.length;counter < size;counter++)
        {
            for(var counter2 = 0, size2 = cells[counter].length;counter2 < size2;counter2++)
                /* When a getNode() call returns empty, it means an empty cell */
                if (cells[counter][counter2].getNode() == null)free.push([counter, counter2]);
        }
        
        if (free.length == 0)return false;
        
        /* Define the spawn item */
        var position = Math.floor(Math.random() * free.length),
            cell     = cells[free[position][0]][free[position][1]];
        
        cell.setNode(new Node($(document.createElement('div')).addClass('node')));
        cell.getNode().getDomElement().html(cell.getNode().getValue());
        cell.getDomElement().append(cell.getNode().getDomElement());

        return true;
    }

    /* Merges two specified Nodes, if they values match */
    function merge(grid, source, target)
    {
        if (grid[target.top] == null || grid[target.top][target.left] == null || grid[target.top][target.left].getNode() == null)return false;
        
        /* If the values match, we can merge the values */
        if (source.getNode().getValue() == grid[target.top][target.left].getNode().getValue())
        { 
            grid[target.top][target.left].getNode().setValue(grid[target.top][target.left].getNode().getValue() * 2);
            source.getNode().getDomElement().remove();
            source.setNode(null);
            return true;
        }
        
        return false;
    }
    
    return this;
}