/**
 *  Represents a cell inside the grid of our game, that can hold
 *  a node and is the primary entity for the entire game logic.
 */
function Cell(element)
{
    var node, domElement = element;
    
    this.getNode        = function(){return node;}
    this.setNode        = function(newNode){node = newNode;}
    this.getDomElement  = function(){return domElement;}
    
    return this;
}