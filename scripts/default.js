(
    function($)
    {
        $(document).ready
        (
            function(args)
            {
                var grid = [], game;
                
                /* Initializing the grid */
                $('#grid tr.row').each
                (
                    function(index, value)
                    {
                        var row = [], cols = $(this).find('td.col');
                        for(var counter = 0, size = cols.length;counter < size;counter++)
                            row.push(new Cell(cols.eq(counter)));
                            
                        grid.push(row);
                        return true;
                    }
                );
                
                /* Starts a new game */
                game = new Game(grid, $('#explanation p')).start();
                
                /* Interaction bindings */
                
                /* Desktop */
                $('#grid').bind
                (
                    'click',
                    function(e)
                    {
                        var target = $(e.target);
                        if(!target.is('td'))target = target.parents('td');
                        if (target.length == 0)return false;
                        
                        var direction = null;
                        
                        switch(true)
                        {
                            case target.hasClass('up'):direction = 'up';break;
                            case target.hasClass('down'):direction = 'down';break;
                            case target.hasClass('left'):direction = 'left';break;
                            case target.hasClass('right'):direction = 'right';break;
                        }
                        
                        game.move(direction);
                        return true;
                    }
                );
                
                /* Mobile */
                if ('ontouchstart' in document)$('#grid').unbind('click');
                $('body').bind
                (
                    'touchstart',
                    function(e)
                    {
                        e.preventDefault();
                        e.stopPropagation();
                        
                        if (!('originalEvent' in e) || !('touches' in e.originalEvent) || e.originalEvent.touches.length == 0)return false;
                        
                        this.coordinates = {x: e.originalEvent.touches[0].screenX, y: e.originalEvent.touches[0].screenY};
                        return true;
                    }
                ).bind
                (
                    'touchend',
                    function(e)
                    {
                        e.preventDefault();
                        e.stopPropagation();
                        
                        if (!('coordinates' in this) || !('originalEvent' in e) || !('changedTouches' in e.originalEvent) || e.originalEvent.changedTouches.length == 0)return false;
                        var diff      = {x: e.originalEvent.changedTouches[0].screenX - this.coordinates.x, y: e.originalEvent.changedTouches[0].screenY - this.coordinates.y},
                            compare   = {x: diff.x < 0 ? diff.x * -1 : diff.x, y: diff.y < 0 ? diff.y * -1 : diff.y},
                            direction = null;

                        switch(true)
                        {
                            /* Horizontal moving */
                            case compare.x > compare.y:
                                if (diff.x < 0)direction = 'left';
                                else direction = 'right';
                            break;
                            /* Vertical moving */
                            case compare.y > compare.x:
                                if (diff.y < 0)direction = 'up';
                                else direction = 'down';
                            break;
                        }

                        game.move(direction);
                        return true;
                    }
                );
                
                /* Removes the loading effect */
                $('#grid').removeClass('loading');
                return true;
            }
        );
        
        return true;
    }
    
)(jQuery);