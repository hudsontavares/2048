2048

This is my JavaScript version of the famous game. It was built using jQuery as framework (I could build it without jQuery, but it helps me to keep more focused on functionalities, rather than cross-browser issues).
To play, you just need to clone this repository and open the index.html file, or simply visit: http://www.nhsistemas.com/mindvalley/2048/.